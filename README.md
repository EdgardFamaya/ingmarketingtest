# README #


### What is this repository for? ###

The ING marketing department is responsible for profiling customers for e-mail targeting based on their monthly spending habits.

* This is a java Back-end application  that is going to provide a service that classifies the customer as per following rules:

Classification	Rules:

Afternoon Person=	Makes over 50% of their transactions in the month after midday (count of transactions)

Big Spender=	Spends over 80% of their deposits every month ($ value of deposits)

Big Ticket Spender=	Makes one or more withdrawals over $1000 in the month

Fast Spender =	Spends over 75% of any deposit within 7 days of making it

Morning Person =	Makes over 50% of their transactions in the month before midday (count of transactions)

Potential Saver =	Spends less than 25% of their deposits every month ($ value of deposits)


## Technical Implementation ##

I  have create a three layer Back-end application based on the Boundary, Control, Entity architecture.

*The Boundary package has the class needed for the high level abstraction of the service.

*The Control package has validators.

*The entity package has all the classes needed for the business logic implementation, dealing with Transactions and reading the datafile into a list.

Functional programming with Java 8 is used mostly to map and reduce the data, initially  all the data is  read from the file and loaded into a List and then for every single service an Stream is used to get the calculation needed.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

## Summary of set up ##
## Configuration ##

*Java 8
*Ensure you have JUnit 4.0 in your path
*Clone this project
*Load it into your favorite Java IDE.

## Dependencies ##
*Move data.txt file to /resources folder



## How to run tests ##

Under /src/test/java8

go to com.ingdirect.au.business.marketing.spendinghabits.test.SpendingHabitTest.java;

Run the test java file.


* Deployment instructions
None

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Edgard Amaya
famaya@yahoo.com
* Other community or team contact