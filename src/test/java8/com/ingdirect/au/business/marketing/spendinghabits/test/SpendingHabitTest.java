package com.ingdirect.au.business.marketing.spendinghabits.test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Test;

import com.ingdirect.au.business.marketing.spendinghabits.boundary.*;
import com.ingdirect.au.business.marketing.spendinghabits.control.*;
import com.ingdirect.au.business.marketing.spendinghabits.entity.CustomerMarketingInfo;
import com.ingdirect.au.business.marketing.spendinghabits.entity.SpendingHabitService;

/**
*
* @author Edgard Amaya
*		Testing of all possible scenarios
*    
*/

public class SpendingHabitTest {
	
	private SpendingHabit	customerSpendingHabits = new SpendingHabit();
	private InputParameter InputParameter = new InputParameter (true,1L,LocalDate.now());
	
	private SpendingHabitService	customerSpendingHabitService = new SpendingHabitService();

	
	@Test
	public void testWrongInputParametersCustomerId() {
		String[] parameters = {"-25","Jun/2016"};
		
		CustomerMarketingInfo custMarketInfo = customerSpendingHabits.getCustomerClassification(parameters);
		
		assertEquals("Wrong CustomerId input Parameter.","Error", custMarketInfo.getStatus() );
		//fail("Not yet implemented");
	}
	
	@Test
	public void testWrongInputParametersDate() {
		String[] parameters = {"25","XXX/2016"};
		
		CustomerMarketingInfo custMarketInfo = customerSpendingHabits.getCustomerClassification(parameters);
		
		assertEquals("Wrong Date input Parameter.","Error", custMarketInfo.getStatus() );
		//fail("Not yet implemented");
	}
	
	@Test
	public void testInputParametersBoundaryOK() {
		String[] parameters = {"25","Jun/2016"};
		
		CustomerMarketingInfo custMarketInfo = customerSpendingHabits.getCustomerClassification(parameters);
		
		assertEquals("Right CustomerId & Date input Parameters.","OK", custMarketInfo.getStatus() );
		//fail("Not yet implemented");
	}
	
	@Test
	public void testCustomerHasTransactions(){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals("Right testCustomerHasTransactions .",true, customerSpendingHabitService.customerHasTransactions(CustomerId,date) );
		//fail("Not yet implemented");
	}
	
	@Test
	public void testNumberOfTransMonth() {
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals( 35L, customerSpendingHabitService.getNumberOfTransMonth(CustomerId,date),0.01 );
		//fail("Not yet implemented");
	}
	
	
	
	@Test
	public void testGetNumberOfTransAfternoon(){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals( 18L, customerSpendingHabitService.getNumberOfTransAfternoon(CustomerId,date),0.01 );
	}
	
	
	public void testGetNumberOfTransMorning(){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals( 17L, customerSpendingHabitService.getNumberOfTransMorning(CustomerId,date),0.01 );
	}
	
	@Test
	public void testGetSumOfDepositsMonth( ){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals( 150.57D, customerSpendingHabitService.getSumOfDepositsMonth(CustomerId,date),0.01 );
	}
	
	@Test
	public void testGetSumOfWithdrawalsMonth( ){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals( -1329.35D, customerSpendingHabitService.getSumOfWithdrawalsMonth(CustomerId,date),0.01 );
	}
	
	@Test
	public void testIsBigTicketCustomer( ){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals("testIsBigTicketCustomer", true, customerSpendingHabitService.isBigTicketCustomer(CustomerId,date) );
	}
	
	@Test
	public void testGetAccountBalance( ){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals( -1178.78D, customerSpendingHabitService.getAccountBalance(CustomerId,date),0.01 );
	}
	
	@Test
	public void testIsBigSpenderCustomer(){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals("testIsBigSpenderCustomer", true, customerSpendingHabitService.isBigSpenderCustomer(CustomerId,date) );

	}
	
	@Test
	public void testIsFastSpenderCustomer(){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals("testIsFastSpenderCustomer", false, customerSpendingHabitService.isFastSpenderCustomer(CustomerId,date) );

	}
	
	@Test
	public void testIsPotencialSaverCustomer(){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals("testIsPotencialSaverCustomer", false, customerSpendingHabitService.isPotencialSaverCustomer(CustomerId,date) );

	}
	
	@Test
	public void testIsPotencialLoanCustomer(){
		Long CustomerId = 25L;
		LocalDateTime date = LocalDateTime.of(2016,06,01,00,00,00);
		
		assertEquals("isPotencialLoanCustomer", false, customerSpendingHabitService.isPotencialLoanCustomer(CustomerId,date) );

	}
	
	@Test
	public void testBalanceInReturnedCustomerObject(){
		String[] parameters = {"25","Jun/2016"};
		
		CustomerMarketingInfo custMarketInfo = customerSpendingHabits.getCustomerClassification(parameters);
		
		assertEquals(-1178.78D, custMarketInfo.getBalance(), 0.01 );
	}

	
	
}
