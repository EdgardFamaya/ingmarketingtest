package com.ingdirect.au.business.marketing.spendinghabits.boundary;

import java.util.ArrayList;
import java.util.List;

import com.ingdirect.au.business.marketing.spendinghabits.control.InputParameter;
import com.ingdirect.au.business.marketing.spendinghabits.control.SpendingHabitValidator;
import com.ingdirect.au.business.marketing.spendinghabits.entity.*;

/**
 *
 * @author Edgard Amaya
 *		Main entry point of the whole service is getCustomerClassification that receives a String of [arameters
 *      with the customerId and the Month of the date t be executed.
 *      
 *      Entry example parameters: "25 Jul/2016"
 */

public class SpendingHabit {
	
	public static void main(String[] args){
		String[] parameters = {"25","Jun/2016"};
		CustomerMarketingInfo customerMarketingInfo;
		
		
		SpendingHabit spendHabit = new SpendingHabit();
		customerMarketingInfo = spendHabit.getCustomerClassification(parameters);
	}
	
	public CustomerMarketingInfo getCustomerClassification(String[] args ){

		InputParameter inputParameter;
		CustomerMarketingInfo custMarketingInfo;
		StringBuilder custClassification= new StringBuilder();
		String status;
		Long numberOfTransactionsMonth=0L;
		Long numberOfTransactionsAfternoon = 0L;
		Long numberOfTransMorning = 0L;
		Double accountBalance = 0D;
		List<Transaction> customerTransactions = new ArrayList<>();
		
		SpendingHabitService	customerSpendingHabitService = new SpendingHabitService();
		
		
		SpendingHabitValidator validatorHabits = new SpendingHabitValidator();
		inputParameter = validatorHabits.isValidInput(args);
		if (inputParameter.getIsValid()){
			status = "OK";
		}else{
			status = "Error";
		}
		
		//Boolean customerHasTransactions = customerSpendingHabitService.customerHasTransactions(inputParameter.getCustomerId(),inputParameter.getStartDate());
		
		
		//Getting Afternoon Person Flag
		numberOfTransactionsMonth= customerSpendingHabitService.getNumberOfTransMonth(inputParameter.getCustomerId(),inputParameter.getStartDate());
		numberOfTransactionsAfternoon = customerSpendingHabitService.getNumberOfTransAfternoon(inputParameter.getCustomerId(),inputParameter.getStartDate());
		if (numberOfTransactionsMonth != 0){
			if (numberOfTransactionsAfternoon / numberOfTransactionsMonth >= .50){
				custClassification.append("Afternoon Person");
			}
		}
		
		//Getting BIG SPENDER
		if (customerSpendingHabitService.isBigSpenderCustomer(inputParameter.getCustomerId(),inputParameter.getStartDate())){
			custClassification.append(", Big Spender");
		}
		
		//Getting BIG Ticket
		if (customerSpendingHabitService.isBigTicketCustomer(inputParameter.getCustomerId(),inputParameter.getStartDate())){
			custClassification.append(", Big Ticket Spender");
		}
		
		//Getting Morning Person Flag
		numberOfTransMorning = numberOfTransactionsMonth - numberOfTransactionsAfternoon;
		
		if (numberOfTransactionsMonth != 0){
			if (numberOfTransMorning / numberOfTransactionsMonth >= .50){
				custClassification.append(", Morning Person");
			}
		}
		
		//Potential Saver
		if(customerSpendingHabitService.isPotencialSaverCustomer(inputParameter.getCustomerId(),inputParameter.getStartDate()) ){
			custClassification.append(", Potencial Saver");
		}
		
		//getting Customer Balance
		accountBalance = customerSpendingHabitService.getAccountBalance(inputParameter.getCustomerId(),inputParameter.getStartDate());
		
		//getting Customer Filtered Transactions
		customerTransactions = customerSpendingHabitService.getCustomerFilteredTrans(inputParameter.getCustomerId(),inputParameter.getStartDate());
		
		
		//Populating return object
		custMarketingInfo = new CustomerMarketingInfo(inputParameter.getCustomerId(), 
														custClassification.toString(), 
														accountBalance, 
														customerTransactions,
														status);
		return custMarketingInfo;
	}
	

}
