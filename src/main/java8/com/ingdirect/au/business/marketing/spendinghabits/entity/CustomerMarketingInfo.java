package com.ingdirect.au.business.marketing.spendinghabits.entity;

import java.util.ArrayList;
import java.util.List;

import com.ingdirect.au.business.marketing.spendinghabits.entity.*;

/**
*
* @author Edgard Amaya
*		Final output class to be returned from the main service.
*    
*/

public class CustomerMarketingInfo {
	private Long customerId;
	private String customerClassification;
	private double balance;
	private List<Transaction> transactions = new ArrayList<>();
	private String status;
	private double numberOfTransactions;
	
	
	public CustomerMarketingInfo(Long customerId, String customerClassification, Double balance,
			List<Transaction> transactions, String status) {
		super();
		this.customerId = customerId;
		this.customerClassification = customerClassification;
		this.balance = balance;
		this.transactions = transactions;
		this.status = status;
		
	}

	
	
	
	public double getNumberOfTransactions() {
		return numberOfTransactions;
	}




	public void setNumberOfTransactions(double numberOfTransactions) {
		this.numberOfTransactions = numberOfTransactions;
	}




	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getCustomerClassification() {
		return customerClassification;
	}

	public void setCustomerClassification(String customerClassification) {
		this.customerClassification = customerClassification;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	@Override
	public String toString(){
		return ("status: " + status + " customerClassification:" + this.getCustomerClassification() + ", balance: "+ this.getBalance() );
	}
}
