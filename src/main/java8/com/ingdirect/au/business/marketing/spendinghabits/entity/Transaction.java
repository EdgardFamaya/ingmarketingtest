package com.ingdirect.au.business.marketing.spendinghabits.entity;


import java.io.Serializable;
import java.time.*;

/**
*
* @author Edgard Amaya
*	Transaction Entity  
*    
*/

public class Transaction implements Serializable{
	private static final long serialVersionUID = 1L;
	private int customerId ;
	private LocalDateTime TransactionDate ;
	private double amount ;
	private String description ;
	
	public Transaction(){}
	
	public Transaction(int customerId, LocalDateTime transactionDate,
			double amount, String description) {
		super();
		this.customerId = customerId;
		TransactionDate = transactionDate;
		this.amount = amount;
		this.description = description;
	}

	protected int getCustomerId() {
		return customerId;
	}

	protected void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	protected LocalDateTime getTransactionDate() {
		return TransactionDate;
	}

	protected void setTransactionDate(LocalDateTime transactionDate) {
		TransactionDate = transactionDate;
	}

	protected double getAmount() {
		return amount;
	}

	protected void setAmount(double amount) {
		this.amount = amount;
	}

	protected String getDescription() {
		return description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}
	
	protected LocalTime getTimeOfTransaction(){
		return(this.getTransactionDate().toLocalTime());
	}
	
	@Override
	public String toString(){
		return ("CustomerId:" + this.getCustomerId() + ", TranDate: "+ this.getTransactionDate() + ", Amount: " + this.getAmount() 
				+ ", Descr: " + this.getDescription());
			
		
	}

}
