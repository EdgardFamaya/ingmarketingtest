package com.ingdirect.au.business.marketing.spendinghabits.control;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
*
* @author Edgard Amaya
*		Validates the input parameters 
*    
*/

public class SpendingHabitValidator {
	public InputParameter isValidInput(String[] args){
		
		InputParameter inputParameter;
		String[] inputSArray;
		Long customerId = 0L;
		LocalDate inputDate=LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
		
		
		if( args.length != 2 ||  args[0] == null ) {
	        throw new IllegalArgumentException("Input has an invalid value. Please use: customerId Jul/2016");
	    }
		
		inputSArray=args;//args.split(" ");

		//validating customerID
    	try {
    		customerId = Long.parseLong(inputSArray[0]);
    		if (customerId <= 0 ){
    			return(new InputParameter(false,customerId, inputDate));
    		}	
    		
    	 } catch (NumberFormatException nfe) {	
    		 return(new InputParameter(false,customerId, inputDate));
    	 }
    	
    	//validating InputDate
    	try {
    		
    		inputDate = LocalDate.parse( "01/"+inputSArray[1],formatter);	
    		
    	 } catch (DateTimeParseException e) {	
    		 return(new InputParameter(false,customerId, inputDate));
    	 }
    	
    	inputParameter = new InputParameter(true,customerId, inputDate);
    	
		return inputParameter;
	}

}
