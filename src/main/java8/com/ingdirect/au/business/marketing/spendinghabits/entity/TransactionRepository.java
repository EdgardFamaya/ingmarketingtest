package com.ingdirect.au.business.marketing.spendinghabits.entity;


import java.util.List;

/**
*
* @author Edgard Amaya
*		Interface to be implemented by every single input data method, initially will be a data file and
*     could be implemented by any other datasource like Database, JSon, etc. 
*    
*/

public interface TransactionRepository <T>  {
	
	List<Transaction>  populateTransactions( ) throws Exception;
	
	Transaction convertRecordToTransaction( String line);
}
