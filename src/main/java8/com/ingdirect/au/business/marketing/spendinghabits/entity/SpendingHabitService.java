package com.ingdirect.au.business.marketing.spendinghabits.entity;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
*
* @author Edgard Amaya
*		The SpendingHabitService class implements all the customer classification logic
*      
*    
*/


public class SpendingHabitService {
	
	
	private TransactionFileImpl transactionFileImpl ;
	private List<Transaction> transactions = new ArrayList<>();
	
	
	public SpendingHabitService(){
		transactionFileImpl = new TransactionFileImpl("resources/data.txt");
		transactions = transactionFileImpl.getTransactions();
	}
	
	public List<Transaction> getCustomerFilteredTrans(Long customerId, LocalDateTime monthToAnalyse){
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
		
		return (transactions
					.stream()
					.filter( predicateTransInMonth)
					.collect(Collectors.toList()));
		
	}
	
	public Boolean customerHasTransactions(Long customerId, LocalDateTime monthToAnalyse){
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
		
		return (transactions
					.stream()
					.anyMatch( predicateTransInMonth));
	}
	
	public Long getNumberOfTransMonth(Long customerId, LocalDateTime monthToAnalyse){
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
		
		return (transactions
					.stream()
					.filter( predicateTransInMonth )
					.count());
	}
	
	public Long getNumberOfTransAfternoon(Long customerId, LocalDateTime monthToAnalyse){
		
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
	
		LocalTime startAfterNoon = LocalTime.of(12, 00,00);
		LocalTime endAfterNoon = LocalTime.of(23, 59,59);
		
		Predicate<Transaction> predicateTransAfternoon = s -> ( s.getTimeOfTransaction().isAfter(startAfterNoon)  
				&& s.getTimeOfTransaction().isBefore(endAfterNoon) );
		
		return (transactions
					.stream()
					.filter(predicateTransInMonth)
					.filter(predicateTransAfternoon)
					.count());
	}
	
	public Long getNumberOfTransMorning(Long customerId, LocalDateTime monthToAnalyse){
		return 0L;
	}
	
	public Double getSumOfDepositsMonth(Long customerId, LocalDateTime monthToAnalyse){
		
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
		
		return (transactions
					.stream()
					.filter(predicateTransInMonth)
					.filter( s -> s.getAmount() > 0  )
					.map(x -> x.getAmount())
					.reduce(0d, (a,b) -> a + b));
	}
	
	
	public Double getSumOfWithdrawalsMonth(Long customerId, LocalDateTime monthToAnalyse){
		
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
		
		return (transactions
				.stream()
				.filter(predicateTransInMonth)
				.filter( s -> s.getAmount() < 0  )
				.map(x -> x.getAmount())
				.reduce(0d, (a,b) -> a + b));
	}
	
	
	public boolean isBigTicketCustomer(Long customerId, LocalDateTime monthToAnalyse){
		
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
		
		return (transactions
					.stream()
					.filter(predicateTransInMonth)
					.anyMatch( s -> s.getAmount() > -1000));
	}
	
	public Double getAccountBalance(Long customerId, LocalDateTime monthToAnalyse){
		
		Predicate<Transaction> predicateTransInMonth = getPredicateTransInMonth( customerId,  monthToAnalyse);
		
		return (transactions
				.stream()
				.filter( predicateTransInMonth)
				.collect(Collectors.summarizingDouble(Transaction::getAmount) )
				.getSum());
	}
	
	public boolean isBigSpenderCustomer(Long customerId, LocalDateTime monthToAnalyse){
		boolean isBigSpenderCustomer = false;
		
		Double sumOfDepositsMonth = getSumOfDepositsMonth(customerId,  monthToAnalyse);
		if (sumOfDepositsMonth > 0){
			Double sumOfWithdrawalsMonth = getSumOfWithdrawalsMonth(customerId,  monthToAnalyse);
			isBigSpenderCustomer = ( -(sumOfWithdrawalsMonth) /sumOfDepositsMonth > .80 );
			
		}
		return(isBigSpenderCustomer);
	}
	
	public boolean isPotencialSaverCustomer(Long customerId, LocalDateTime monthToAnalyse){
		boolean isPotencialSaverCustomer = false;
		
		Double sumOfDepositsMonth = getSumOfDepositsMonth(customerId,  monthToAnalyse);
		if (sumOfDepositsMonth > 0){
			Double sumOfWithdrawalsMonth = getSumOfWithdrawalsMonth(customerId,  monthToAnalyse);
			isPotencialSaverCustomer = ( -(sumOfWithdrawalsMonth) /sumOfDepositsMonth < .25 );
			
		}
		
		return(isPotencialSaverCustomer);
	}
	
	public boolean isFastSpenderCustomer(Long customerId, LocalDateTime monthToAnalyse){
		return false;
	}
	
	public boolean isPotencialLoanCustomer(Long customerId, LocalDateTime monthToAnalyse){
		return false;
	}
	
	public Predicate<Transaction> getPredicateTransInMonth(Long customerId, LocalDateTime monthToAnalyse){
		LocalDateTime endDate = monthToAnalyse.plusMonths(1).minusSeconds(1);
		
		return( x -> ( ( x.getCustomerId() == customerId )
	            && (x.getTransactionDate().isAfter(monthToAnalyse) && 
	           	 x.getTransactionDate().isBefore(endDate) )));
		
	}

}
