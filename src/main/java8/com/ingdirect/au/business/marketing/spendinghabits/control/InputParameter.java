package com.ingdirect.au.business.marketing.spendinghabits.control;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
*
* @author Edgard Amaya
*		Initial input parameters to the app.
*    
*/
public class InputParameter {
	private Boolean isValid;
	private Long customerId;
	private LocalDate dateToAnalize;
	private LocalDateTime startDate;
	private LocalTime startTime = LocalTime.of(0,0,0);
	
	
	public InputParameter(Boolean isValid, Long customerId, LocalDate dateToAnalize) {
		super();
		this.isValid = isValid;
		this.customerId = customerId;
		this.dateToAnalize = dateToAnalize;
		this.startDate = LocalDateTime.of(dateToAnalize, startTime);
	}
	
	public LocalDateTime getStartDate(){
		return startDate;
	}
	
	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public LocalDate getdateToAnalize() {
		return dateToAnalize;
	}
	public void setdateToAnalize(LocalDate dateToAnalize) {
		this.dateToAnalize = dateToAnalize;
	}
	

}
