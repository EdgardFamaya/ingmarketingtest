package com.ingdirect.au.business.marketing.spendinghabits.entity;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
*
* @author Edgard Amaya
*		Reads the input file and loads the data into a  List  
*    
*/

public class TransactionFileImpl implements TransactionRepository<File>{

	private Path path;
	private List<Transaction> transactions = new ArrayList<>();
	
	public TransactionFileImpl(String DataFileName){
		path = Paths.get(DataFileName);
		//TransactionFileImpl spendHabit= new TransactionFileImpl(DataFileName);
		try{
			transactions = populateTransactions();
		}catch(IOException e){
			handleException(e);
		}
	}
	
	
	public List<Transaction> getTransactions(){
		return transactions;
	}
	
	
	public List<Transaction> populateTransactions( ) throws  IOException{
		
		List<Transaction> transactions = new ArrayList<>();
		
		//System.out.println("File exists: " +  Files.exists(Paths.get("resources","data.txt")));
		
		//Path path = Paths.get("resources","data.txt");
		try {
			transactions = Files
				.lines(path)
				.map(s -> convertRecordToTransaction(s) )
				.collect(Collectors.toList());
			
		}catch (IOException e){
			
		}
		return transactions;
	}
	
	
	public  Transaction convertRecordToTransaction( String line){
		
		Transaction t = new Transaction();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy h:m:s a");
		
        String[] tokens = line.split(",");
      
        if (line.toLowerCase().equals("customerid,date,amount,description")){ //Not processing Header
        	return(t); 
        }
        
		 t.setCustomerId(Integer.parseInt(tokens[0]));
		 t.setTransactionDate(LocalDateTime.parse( tokens[1],formatter));
		 t.setAmount(Double.parseDouble(tokens[2]));
		 t.setDescription(tokens[3]);
		 
		 return (t);
	}
	
	private static void handleException (Exception e){
		e.printStackTrace();
		throw new RuntimeException();
	}

	
}
